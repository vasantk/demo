package com.btc.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	@GetMapping("/demo/getUserById")
	public String getUser() {
		return "I am here!!";
	}

}
